# PaPy Bot

Ceci est un projet étudiant réalisé pour le projet 7 du cours Python d'OpenClassrooms.

Le but de ce projet est de développer un chatbot pour informer les utilisateurs sur un lieu.

Suivez [ce lien](https://smallpybot.herokuapp.com/) pour utiliser l'application et profitez-en !

[![image](https://img.shields.io/github/license/Sam-prog-sudo/sam.github.io?style=flat-square)](https://gitlab.com/oc-sam/papy-bot/-/blob/main/LICENSE)

## Table des matières  

- [Contraintes](#contraintes)  
- [Cahier des charges](#cahier-des-charges)  
  - [Fonctionnalités](#fonctionnalités)  
  - [Parcours utilisateur](#parcours-utilisateur)  
- [Améliorations possibles](#améliorations-possibles)  
- [License](#license)  

---

## Contraintes

- Pas de framework javascript
- Pas de framework css
- Pas de wrapper pour l'accès aux API

---

## Cahier des charges

### Fonctionnalités

- Interactions en AJAX : l'utilisateur envoie sa question en appuyant sur entrée et la réponse s'affiche directement dans l'écran, sans recharger la page.
- Vous utiliserez l'API de Google Maps et celle de Media Wiki.
- Rien n'est sauvegardé. Si l'utilisateur charge de nouveau la page, tout l'historique est perdu.

### Parcours utilisateur

L'utilisateur ouvre son navigateur et entre l'URL que vous lui avez fournie. Il arrive devant une page contenant les éléments suivants :

- header : logo et phrase d'accroche
- zone centrale : zone vide (qui servira à afficher le dialogue) et champ de formulaire pour envoyer une question.
- footer : votre prénom & nom, lien vers votre repository Github et autres réseaux sociaux si vous en avez

L'utilisateur tape "Salut GrandPy ! Est-ce que tu connais l'adresse d'OpenClassrooms ?" dans le champ de formulaire puis appuie sur la touche Entrée. Le message s'affiche dans la zone du dessus qui affiche tous les messages échangés. Une icône tourne pour indiquer que GrandPy est en train de réfléchir.

Puis un nouveau message apparaît : "Bien sûr mon poussin ! La voici : 7 cité Paradis, 75010 Paris." En-dessous, une carte Google Maps apparaît également avec un marqueur indiquant l'adresse demandée.

GrandPy envoie un nouveau message : "Mais t'ai-je déjà raconté l'histoire de ce quartier qui m'a vu en culottes courtes ? La cité Paradis est une voie publique située dans le 10e arrondissement de Paris. Elle est en forme de té, une branche débouche au 43 rue de Paradis, la deuxième au 57 rue d'Hauteville et la troisième en impasse. [En savoir plus sur Wikipedia](https://fr.wikipedia.org/wiki/Cit%C3%A9_Paradis)"

---

## Améliorations possibles

Cette liste est non-exhaustive:

- Améliorer la qualité du code js
- Utilisé un framework css.
- Classe de gestion d'erreur avec séparation des constantes (textes d'erreurs).
- Faire des tests fonctionnels.
- Rajouter des réponses différentes pour GrandPy bot.
- Utiliser une IA pour améliorer le parsing.
- Utiliser wikimedia english avec l'API google translate pour retourner plus de résultats.

## License

**[MIT license](https://gitlab.com/oc-sam/papy-bot/-/blob/main/LICENSE)**
