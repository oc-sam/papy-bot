import requests
from requests.exceptions import HTTPError
from settings.params import GEOCODE


class ApiMap:

    def __init__(self, keywords: str) -> None:
        self.keywords = keywords
        self.url = GEOCODE["url"]
        self.payload = self.get_payload(self.keywords)
        self.address, self.lat, self.lng = self.get_infos()

    @staticmethod
    def get_payload(keywords: str) -> dict:
        """
        Build request payload with parsed keywords.

        Args:
            keywords (str): parsed keywords

        Returns:
            dict: payload
        """
        payload = GEOCODE['params']
        payload["address"] = keywords
        return payload

    def get_infos(self) -> tuple:
        """
        Request google api to fetch localization data.

        - it makes the request,
        - extract full text address, latitude and longitude.

        Returns:
            tuple[str, float|str, float|str]: address, latitude and longitude |
            error title, error description and error probable cause.
        """
        res = requests.get(self.url, params=self.payload)
        try:
            data_res = res.json()["results"][0]
            try:
                infos = {
                    "address":  data_res["formatted_address"],
                    "lat": data_res["geometry"]["location"]["lat"],
                    "lng": data_res["geometry"]["location"]["lng"]
                }
            except data_res["status"] == "ZERO_RESULTS":
                ()
                return (
                    "Error MAPS",
                    "Invalid question. "
                    "Maps API couldn't find any result for: ",
                    self.keywords
                )
            return tuple(infos[key] for key in infos.keys())

        except (IndexError, KeyError, ValueError, HTTPError) as e:
            ()
            return (
                "Error MAPS", "Something went wrong with maps API: ", str(e)
            )
