import requests
from requests.exceptions import HTTPError

from settings.params import WIKI


class ApiWiki:
    def __init__(self, place):
        self.url = WIKI['url']
        self.lat, self.lng = place.lat, place.lng
        self.payload = self.get_payload((self.lat, self.lng))
        self.extract, self.url, self.title = self.get_extract()

    def get_extract(self):
        """
        Request wiki api to fetch place related data.

        - it makes the request,
        - extract page summary, page full url and page title.

        Returns:
            tuple[str, str, str]: page summary, page full url and page title |
            error title, error description and error probable cause.
        """
        result = requests.get(self.url, params=self.payload)
        try:
            response = result.json()
            try:
                pages = response['query']['pages']
                place = self.filter_results(pages)
                return (
                    place['extract'],
                    place['fullurl'],
                    place['title']
                )
            except any(['warnings' in response, 'error' in response]):
                return (
                    'Error WIKI',
                    'Invalid coordinates. '
                    'Cannot find place related to those coordinates: ',
                    (self.lat, self.lng)
                )
        except (HTTPError, ValueError, KeyError, BaseException) as e:
            return (
                "Error WIKI", "Something went wrong with wiki API: ", str(e)
            )

    @staticmethod
    def filter_results(res_dict):
        """
        filter results by page index.

        Args:
            res_dict (dict): all returned pages.

        Returns:
            dict: place related page data
        """
        idx_id = [
            (data_res['index'], key_res)
            for key_res, data_res in res_dict.items()
        ]
        return res_dict[min(idx_id)[1]]

    @staticmethod
    def get_payload(lat_lng: tuple):
        """
        Build request payload with latitude and longitude.

        Args:
            lat_lng (tuple): latitude and longitude

        Returns:
            dict: payload
        """
        payload = WIKI['params']
        payload["ggscoord"] = '|'.join(
            [
                str(lat_lng[0]),
                str(lat_lng[1])
            ]
        )
        return payload
