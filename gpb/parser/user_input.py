from typing import List

from gpb.ressources.stopchars import PUNCTUATION, STOP


class InputParser:
    def __init__(self, unparsed: str):
        self.toparse = unparsed.lower()
        self.parsed = self.clean_up()

    def clean_up(self) -> str:
        """
        clean_up user input.

        It cleans up user input from punctuations and stop words.

        Returns:
            str: cleaned up words as one string of characters.
        """
        words_list = self.replace_punctuation(self.toparse)
        stop_words = sum(STOP.values(), [])
        cleaned_words = [word for word in words_list if word not in stop_words]
        return ' '.join(cleaned_words)

    @staticmethod
    def replace_punctuation(text: str) -> List[str]:
        """
        replace punctuation in a string.

        Arg:
            text (str): text to be cleaned

        Returns:
            List[str]: list of words without punctuations.
        """
        for punc in PUNCTUATION:
            text = text.replace(punc, ' ')
        return text.split()
