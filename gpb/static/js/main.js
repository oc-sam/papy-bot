let timeout;

function myFunction() {
    timeout = setTimeout(alertFunc, 3000);
}

function alertFunc() {
    alert("Hello!");
}

function update_chat(infos) {
    let json = infos.response
    let question = infos.question
    let answerElts = document.getElementById('answer');
    let map_id = "map" + Date.now()
    if (json.hasOwnProperty('error')) {
        let balises = `
            <p class="question_display">
            ${question}
            </p>
            <p class="response">
                Je ne trouve pas ce lieu mon enfant ...
            </p>
    `;
    console.error(json.error_text, json.error_var);
    answerElts.insertAdjacentHTML("beforeend", balises);
    }
    else {
        let balises = `
            <p class="question_display">
                ${question}
            </p>
            <p class="response place_display">
            Oh, ça me rappel de bon souvenir ...<br>
            Laisse moi te donner plus d'information sur: <br>
                ${json.place_name}
            </p>
            <p class="response address_display">
            Et si tu cherches à t'y rendre, voilà l'adresse:<br> 
                ${json.address}
            </p>
            <p class="response about_display">
            Mais t'ai-je déjà raconté l'histoire de cet endroit qui m'a vu en culottes courtes ?<br>
            </p>
            <p class="response about_display">
                ${json.about}
            </p>
            <p class="response link_display">
                <a href=${json.url} 
                    target="_blank"
                    rel="noopener noreferrer"
                >
                [En savoir plus sur Wikipedia]
                </a>
            </p>
            <div id=${map_id} class="response map">
            </div>
        `;
        answerElts.insertAdjacentHTML("beforeend", balises);
        function initMap() {
            const myplace = { lat: json.latitude, lng: json.longitude };
            const map = new google.maps.Map(document.getElementById(map_id), {
              zoom: 16,
              center: myplace,
            });
            const marker = new google.maps.Marker({
              position: myplace,
              map: map,
            });
          };
        initMap()
    }
};


let monform = document.getElementById('question-form');

monform.onsubmit = function (e) {
    e.preventDefault();
    let spinner = document.getElementById('spinner')
    spinner.classList.remove('hide')
    data_form = new FormData(monform)
    fetch('/answer',
        {
            method: 'POST',
            body: data_form
        }
    )
        .then(document.getElementById('user_input').value='')
        .then(function (response) {
            response.json().then(
                function (resp) {
                    setTimeout(function () {
                        spinner.classList.add('hide');
                        update_chat({"question": data_form.get("user_input"), "response": resp})
                    }, 2000)
                }
            )
        })
        .catch(function () {
            alert("Une erreur s'est produite")
        })
}
