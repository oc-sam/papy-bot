from api.maps import ApiMap
from api.wiki import ApiWiki
from flask import Flask, Response, jsonify, request
from flask.templating import render_template

from gpb.parser.user_input import InputParser

app = Flask(__name__)


@app.route("/", methods=["GET"])
def landing_page() -> str:
    """
    view function for homepage.

    Returns:
        str: rendered template
    """
    return render_template("landing_page.html")


@app.route('/answer', methods=["POST"])
def ask() -> Response:
    """
    View function to process user input.

    User input is parsed for stop words.
    Then keywords are sent through google api
    to retreive infos about its location.
    And Finally, gps coordinates are sent to wiki api
    to fetch a summary about the place.

    Returns:
        Response | None: response object.
    """
    if "user_input" in request.form:
        error_titles = ("Error WIKI", "Error MAPS")
        dirty_input = request.form['user_input']
        search = InputParser(dirty_input).parsed
        place = ApiMap(search)
        if place.address in error_titles:
            return jsonify({
                'error': place.address,
                "error_text": place.lat,
                "error_var": place.lng
                })
        wiki = ApiWiki(place)
        if wiki.extract in error_titles:
            return jsonify({
                'error': wiki.extract,
                "error_text": wiki.url,
                "error_var": wiki.title
                })
        res = dict(
            zip(
                (
                    'user_input',
                    'address',
                    'latitude',
                    'longitude',
                    'place_name',
                    'about',
                    'url'
                ),
                (
                    dirty_input,
                    place.address,
                    place.lat,
                    place.lng,
                    wiki.title,
                    wiki.extract,
                    wiki.url,
                )
            )
        )
        return jsonify(res)
