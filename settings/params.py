import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

MAP_API_KEY = os.environ.get("MAP_API_KEY")

GEOCODE = {
    "url": "https://maps.googleapis.com/maps/api/geocode/json",
    "params": {
        "key": MAP_API_KEY,
        "address": "",
        "fields": "formatted_address,geometry,name",
        "inputtype": "textquery",
        "language": "fr",
    }
}

MAPS = {
    "url": "https://www.google.com/maps/embed/v1/place",
    "params": {
        "key": MAP_API_KEY,
        "q": "",
        "language": "fr",
    }
}

WIKI = {
    "url": "https://fr.wikipedia.org/w/api.php",
    "params": {
        "action": "query",
        "prop": "extracts|info",
        "inprop": "url",
        "explaintext": True,
        "exintro": True,
        "generator": "geosearch",
        "ggsradius": 1000,
        "ggscoord": "",
        "format": "json",
    }
}
