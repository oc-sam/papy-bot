from gpb.parser.user_input import InputParser


class MockInputParser:
    """
    declr toute les méthode utilisés dans le test
    """
    @staticmethod
    def replace_punctuation():
        return [
            'j',
            'aimerais',
            'savoir',
            'ou',
            'se',
            'trouve',
            'blabla'
        ]


def test_replace_punctuation():
    dirty_text = "salut ! comment ça va, aujourd'hui ?"
    incorrect_cleaned_words = [
        'salut', '!', 'comment', 'ça', 'va,', "aujourd'hui", '?'
    ]
    correct_cleaned_words = ['salut', 'comment', 'ça', 'va', "aujourd", "hui"]
    assert InputParser.replace_punctuation(
        dirty_text
    ) != incorrect_cleaned_words
    assert InputParser.replace_punctuation(
        dirty_text
    ) == correct_cleaned_words


def test_remove_stop_words(mocker):
    text = "j'aimerais savoir ou se trouve la tour eiffel"
    expected = 'tour eiffel'
    mocker.patch(
        'gpb.parser.user_input.InputParser',
        return_response=MockInputParser()
    )
    result = InputParser(text).clean_up()
    assert result == expected
