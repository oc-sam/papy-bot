from api.maps import ApiMap
from settings.params import GEOCODE
from tests.mock.mock_maps import (correct_infos, correct_payload, keywords,
                                  path_to_response)
from tests.mock.tools import MockResponse


def test_get_payload(mocker):
    mocker.patch.dict(GEOCODE['params'], {'key': 'secret_key'})
    assert ApiMap.get_payload(keywords) == correct_payload


def test_get_extract_success(mocker):
    mocker.patch(
        'requests.get',
        return_value=MockResponse(path_to_response)
    )
    result = ApiMap(keywords).get_infos()
    assert result == correct_infos
