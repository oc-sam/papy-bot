from api.wiki import ApiWiki
from tests.mock.mock_wiki import (correct_filtered_res, correct_payload,
                                  expected_get_extract_res, path_to_response,
                                  payload_param)
from tests.mock.tools import MockPlaceObj, MockResponse, unfiltered_results


def test_get_payload():
    assert ApiWiki.get_payload(payload_param) == correct_payload


def test_filter_results():
    assert ApiWiki.filter_results(
        unfiltered_results(path_to_response)
    ) == correct_filtered_res


def test_get_extract_success(mocker):
    mocker.patch(
        'requests.get',
        return_value=MockResponse(path_to_response)
    )
    mocker.patch(
        'api.wiki.ApiWiki.filter_results',
        return_value=correct_filtered_res
    )
    a_place = MockPlaceObj()
    assert ApiWiki(a_place).get_extract() == expected_get_extract_res
