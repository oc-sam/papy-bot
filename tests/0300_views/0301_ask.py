# flake8: noqa
from tests.conftest import client


expected_data_in_response = {
    'about':
    "La tour Eiffel  est une tour de fer puddlé de 324 mètres de hauteur (avec antennes) située à Paris, à l’extrémité nord-ouest du parc du Champ-de-Mars en bordure de la Seine dans le 7e arrondissement. Son adresse officielle est 5, avenue Anatole-France.\nConstruite en deux ans par Gustave Eiffel et ses collaborateurs pour l'Exposition universelle de Paris de 1889, célébrant le centenaire de la Révolution française, et initialement nommée « tour de 300 mètres », elle est devenue le symbole de la capitale française et un site touristique de premier plan : il s’agit du troisième site culturel français payant le plus visité en 2015, avec 5,9 millions de visiteurs en 2016. Depuis son ouverture au public, elle a accueilli plus de 300 millions de visiteurs.\nD’une hauteur de 312 mètres à l’origine, la tour Eiffel est restée le monument le plus élevé du monde pendant quarante ans. Le second niveau du troisième étage, appelé parfois quatrième étage, situé à 279,11 mètres, est la plus haute plateforme d'observation accessible au public de l'Union européenne et la deuxième plus haute d'Europe, derrière la tour Ostankino à Moscou culminant à 337 mètres. La hauteur de la tour a été plusieurs fois augmentée par l’installation de nombreuses antennes. Utilisée dans le passé pour de nombreuses expériences scientifiques, elle sert aujourd’hui d’émetteur de programmes radiophoniques et télévisés.",
    'address': 'Champ de Mars, 5 Av. Anatole France, 75007 Paris, France',
    'latitude': 48.85837009999999,
    'longitude': 2.2944813,
    'place_name': 'Tour Eiffel',
    'url': 'https://fr.wikipedia.org/wiki/Tour_Eiffel',
    'user_input': 'tour eiffel'
}


def test_landing_page_should_status_code_ok(client):
    response = client.get('/')
    assert response.status_code == 200


def test_ask_success_status_code_and_data(client):
    response = client.post('/answer', data=dict(user_input='tour eiffel'))
    assert response.status_code == 200
    assert response.get_json() == expected_data_in_response
