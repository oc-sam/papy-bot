path_to_response = "tests/mock/maps_correct_res.json"
keywords = "tour eiffel"

correct_payload = {
        "key": "secret_key",
        "address": "tour eiffel",
        "fields": "formatted_address,geometry,name",
        "inputtype": "textquery",
        "language": "fr",
    }
incorrect_payload = {
        "key": "secret_key",
        "address": "foobar",
        "fields": "formatted_address,geometry,name",
        "inputtype": "textquery",
        "language": "fr",
    }
correct_res = {
    'results': [
        {
            'address_components': [
                {
                    'long_name': '5',
                    'short_name': '5',
                    'types': ['street_number']
                },
                {
                    'long_name': 'Avenue Anatole France',
                    'short_name': 'Av. Anatole France',
                    'types': ['route']
                },
                {
                    'long_name': 'Paris',
                    'short_name': 'Paris',
                    'types': ['locality', 'political']
                },
                {
                    'long_name':
                    'Département de Paris',
                    'short_name':
                    'Département de Paris',
                    'types': ['administrative_area_level_2', 'political']
                },
                {
                    'long_name':
                    'Île-de-France',
                    'short_name':
                    'IDF',
                    'types': ['administrative_area_level_1', 'political']
                },
                {
                    'long_name': 'France',
                    'short_name': 'FR',
                    'types': ['country', 'political']
                },
                {
                    'long_name': '75007',
                    'short_name': '75007',
                    'types': ['postal_code']
                }
            ],
            'formatted_address':
            'Champ de Mars, 5 Av. Anatole France, 75007 Paris, France',
            'geometry': {
                'location': {
                    'lat': 48.85837009999999,
                    'lng': 2.2944813
                },
                'location_type': 'ROOFTOP',
                'viewport': {
                    'northeast': {
                        'lat': 48.8597190802915,
                        'lng': 2.295830280291502
                    },
                    'southwest': {
                        'lat': 48.8570211197085,
                        'lng': 2.293132319708498
                    }
                }
            },
            'place_id':
            'ChIJLU7jZClu5kcR4PcOOO6p3I0',
            'plus_code': {
                'compound_code': 'V75V+8Q Paris, France',
                'global_code': '8FW4V75V+8Q'
            },
            'types': [
                'establishment',
                'point_of_interest',
                'tourist_attraction'
            ]
        }
    ],
    'status': 'OK'
}
correct_infos = (
    'Champ de Mars, 5 Av. Anatole France, 75007 Paris, France',
    48.85837009999999,
    2.2944813,
    )


class MockApiMap:
    def __init__(self) -> None:
        self.keywords = keywords
        self.url = "https://maps.googleapis.com/maps/api/geocode/json"
        self.payload = self.get_payload(self.keywords)
        self.address, self.lat, self.lng, self.id = self.get_infos()

    def get_infos(self):
        return correct_infos

    @staticmethod
    def get_payload(keywords):
        return correct_payload
