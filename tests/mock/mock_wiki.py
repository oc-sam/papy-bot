path_to_response = 'tests/mock/wiki_correct_res.json'

correct_filtered_res = {
    'pageid': 1359783,
    'ns': 0,
    'title': 'Tour Eiffel',
    'index': -1,
    'extract':
    "La tour Eiffel  est une tour de fer puddlé de 324 mètres de hauteur (avec antennes) située à Paris, à l’extrémité nord-ouest du parc du Champ-de-Mars en bordure de la Seine dans le 7e arrondissement. Son adresse officielle est 5, avenue Anatole-France.\nConstruite en deux ans par Gustave Eiffel et ses collaborateurs pour l'Exposition universelle de Paris de 1889, célébrant le centenaire de la Révolution française, et initialement nommée « tour de 300 mètres », elle est devenue le symbole de la capitale française et un site touristique de premier plan : il s’agit du troisième site culturel français payant le plus visité en 2015, avec 5,9 millions de visiteurs en 2016. Depuis son ouverture au public, elle a accueilli plus de 300 millions de visiteurs.\nD’une hauteur de 312 mètres à l’origine, la tour Eiffel est restée le monument le plus élevé du monde pendant quarante ans. Le second niveau du troisième étage, appelé parfois quatrième étage, situé à 279,11 mètres, est la plus haute plateforme d'observation accessible au public de l'Union européenne et la deuxième plus haute d'Europe, derrière la tour Ostankino à Moscou culminant à 337 mètres. La hauteur de la tour a été plusieurs fois augmentée par l’installation de nombreuses antennes. Utilisée dans le passé pour de nombreuses expériences scientifiques, elle sert aujourd’hui d’émetteur de programmes radiophoniques et télévisés.",  # noqa
    'contentmodel': 'wikitext',
    'pagelanguage': 'fr',
    'pagelanguagehtmlcode': 'fr',
    'pagelanguagedir': 'ltr',
    'touched': '2022-01-13T18:52:34Z',
    'lastrevid': 189449360,
    'length': 140377,
    'fullurl': 'https://fr.wikipedia.org/wiki/Tour_Eiffel',
    'editurl':
    'https://fr.wikipedia.org/w/index.php?title=Tour_Eiffel&action=edit',
    'canonicalurl': 'https://fr.wikipedia.org/wiki/Tour_Eiffel'
}
correct_payload = {
    'action': 'query',
    'prop': 'extracts|info',
    'inprop': 'url',
    'explaintext': True,
    'exintro': True,
    'generator': 'geosearch',
    'ggsradius': 1000,
    'ggscoord': '42|43',
    'format': 'json'
}
incorrect_payload = {
    'action': 'query',
    'prop': 'extracts|info',
    'inprop': 'url',
    'explaintext': True,
    'exintro': True,
    'generator': 'geosearch',
    'ggsradius': 1000,
    'ggscoord': '43|42',
    'format': 'json'
}

payload_param = (42, 43)

expected_get_extract_res = (
    "La tour Eiffel  est une tour de fer puddlé de 324 mètres de hauteur (avec antennes) située à Paris, à l’extrémité nord-ouest du parc du Champ-de-Mars en bordure de la Seine dans le 7e arrondissement. Son adresse officielle est 5, avenue Anatole-France.\nConstruite en deux ans par Gustave Eiffel et ses collaborateurs pour l'Exposition universelle de Paris de 1889, célébrant le centenaire de la Révolution française, et initialement nommée « tour de 300 mètres », elle est devenue le symbole de la capitale française et un site touristique de premier plan : il s’agit du troisième site culturel français payant le plus visité en 2015, avec 5,9 millions de visiteurs en 2016. Depuis son ouverture au public, elle a accueilli plus de 300 millions de visiteurs.\nD’une hauteur de 312 mètres à l’origine, la tour Eiffel est restée le monument le plus élevé du monde pendant quarante ans. Le second niveau du troisième étage, appelé parfois quatrième étage, situé à 279,11 mètres, est la plus haute plateforme d'observation accessible au public de l'Union européenne et la deuxième plus haute d'Europe, derrière la tour Ostankino à Moscou culminant à 337 mètres. La hauteur de la tour a été plusieurs fois augmentée par l’installation de nombreuses antennes. Utilisée dans le passé pour de nombreuses expériences scientifiques, elle sert aujourd’hui d’émetteur de programmes radiophoniques et télévisés.",  # noqa
    'https://fr.wikipedia.org/wiki/Tour_Eiffel',
    'Tour Eiffel',
)
