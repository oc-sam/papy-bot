import json


def load_json(path_to_json):
    with open(path_to_json) as json_file:
        return json.load(json_file)


class MockResponse:
    def __init__(
        self,
        path_to_json_response: str,
        status_code: int = 200
    ) -> None:
        self.status_code = status_code
        self.path = path_to_json_response

    def get_json(self):
        return load_json(self.path)
    json = get_json


class MockPlaceObj:
    lat, lng = (
            48.85837009999999,
            2.2944813,
        )


def unfiltered_results(path):
    data = load_json(path)
    return data['query']['pages']
